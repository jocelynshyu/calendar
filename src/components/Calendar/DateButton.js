import styled from 'styled-components';

const Date = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  @media (hover: hover) {
    transition: background 0.3s;

    &:hover {
      background: var(--hover);
    }
  }

  &.disabled {
    color: var(--disabled);
    cursor: not-allowed;
  }

  &.today{
    background: var(--today);
  }

  &.active{
    color: var(--background);
    background: var(--active);

    @media (hover: hover) {
      transition: opacity 0.3s;

      &:hover {
        opacity: 0.8;
      }
    }
  }
`;

function DateButton({ date, disabled, start, end, today }) {
  const isActive = start === date || (start < date && date <= end);
  const isToday = today === date;
  const text = parseInt(date.slice(-2));
  const className = [disabled ? 'disabled' : '', isToday ? 'today' : '', isActive ? 'active' : ''].filter(t => t).join(' ');
  return <Date className={className} data-date={date}>{text}</Date>;
};

export default DateButton;
