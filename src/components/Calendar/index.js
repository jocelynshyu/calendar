import { useState, useCallback, useEffect, useMemo } from 'react';
import styled from 'styled-components';

import { getFirstDate, getDateString } from '../../libs/date';
import Header from './Header';
import Body from './Body';

const Wrapper = styled.div`
  width: 350px;
  font-size: 16px;
  background: var(--background);

  --background: #fff;
  --hover: #e6e6e6;
  --disabled: #757575;
  --today: #ffff76;
  --active: #006edc;
`;

function Calendar({ onChange }) {
  const today = useMemo(() => new Date(), []);

  const [year, setYear] = useState(today.getFullYear());
  const [month, setMonth] = useState(today.getMonth());

  const [start, setStart] = useState(null);
  const [end, setEnd] = useState(null);

  const onMonthChange = useCallback((direction) => {
    const nextFirst = new Date(getFirstDate(year, month).setMonth(month + direction));

    setYear(nextFirst.getFullYear());
    setMonth(nextFirst.getMonth());
  }, [year, month]);

  const onDateChange = useCallback((currentDate) => {
    if (end) {
      setStart(currentDate);
      setEnd(null);
      return;
    }

    if (!start || currentDate < start) {
      setStart(currentDate);
      return;
    }

    setEnd(currentDate);
  }, [start, end]);

  useEffect(() => onChange(start, end), [start, end, onChange]);

  return (
    <Wrapper>
      <Header month={month} year={year} onChange={onMonthChange} />
      <Body
        start={start}
        end={end}
        year={year}
        month={month}
        onChange={onDateChange}
        today={getDateString(today)}
      />
    </Wrapper>
  );
}

export default Calendar;
