import { useCallback } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 16px;
`;

const PrevButton = styled.button`
  display: flex;
  width: 44px;
  height: 44px;
  padding: 0;
  border: 0;
  outline: 0;
  background: var(--background);;
  cursor: pointer;

  @media (hover: hover) {
    transition: background 0.3s;

    &:hover {
      background: var(--hover);
    }
  }

  &::before {
    content: "";
    width: 6px;
    height: 6px;
    margin: auto;
    border-left: 1px solid;
    border-bottom: 1px solid;
    transform: translate(20%, 0) rotate(45deg);
  }
`;

const NextButton = styled(PrevButton)`
  transform: rotate(180deg);
`;

function CalendarHeader({ year, month, onChange }) {
  const title = `${year} 年 ${month + 1} 月`;

  const onPrev = useCallback(() => onChange(-1), [onChange]);
  const onNext = useCallback(() => onChange(1), [onChange]);

  return (
    <Wrapper>
      <PrevButton onClick={onPrev} />
      <span>{title}</span>
      <NextButton onClick={onNext} />
    </Wrapper>
  );
};

export default CalendarHeader;
