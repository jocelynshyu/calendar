import { useCallback, useMemo } from 'react';
import styled from 'styled-components';

import { getFirstDate, getLastDate, getRelativeDate, formateDate } from '../../libs/date';
import DateButton from './DateButton';

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: repeat(7, 1fr);
  grid-auto-rows: 36px;
`;

function CalendarBody({ start, end, year, month, today, onChange }) {
  const first = useMemo(() => getFirstDate(year, month), [year, month]);
  const last = useMemo(() => getLastDate(year, month), [year, month]);

  const getDateNodes = useCallback((length, startDate) => {
    const firstDate = startDate?.getDate() || 1;
    const arr = new Array(length).fill(0).map((n, i) => firstDate + i);

    const y = startDate?.getFullYear() || year;
    const m = startDate?.getMonth() || month;

    return arr.map((d) => {
      const dateString = formateDate(y, m, d);

      return (
        <DateButton
          key={dateString}
          date={dateString}
          disabled={!!startDate}
          start={start}
          end={end}
          today={today}
        />
      )
    });
  }, [start, end, year, month, today]);

  const current = useMemo(() => getDateNodes(last.getDate()), [last, getDateNodes]);

  const prefix = useMemo(() => {
    const length = first.getDay() % 7;
    const firstDate = getRelativeDate(first, length * -1);
    return getDateNodes(length, firstDate);
  }, [first, getDateNodes]);

  const suffix = useMemo(() => {
    const length = 6 - (last.getDay() % 7);
    const firstDate = getRelativeDate(last, 1);
    return getDateNodes(length, firstDate);
  }, [last, getDateNodes]);

  const onClick = useCallback((event) => {
    const target = event.target;
    const date = target.dataset.date;

    if (!date || target.classList.contains('disabled')) return;
    onChange(date);
  }, [onChange]);

  return (
    <Wrapper onClick={onClick}>
      {prefix}
      {current}
      {suffix}
    </Wrapper>
  );
};

export default CalendarBody;
