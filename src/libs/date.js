export function toDigit(number, digit = 2) {
  const numberString = `${number}`;
  if (numberString.length === digit) return numberString;
  const prev = new Array(digit - numberString.length).fill('0').join('');
  return `${prev}${numberString}`;
}

export function formateDate(year, month, date) {
  return `${year}-${toDigit(month + 1)}-${toDigit(date)}`;
}

export function getDateString(time) {
  const year = time.getFullYear();
  const month = time.getMonth();
  const date = time.getDate();
  return formateDate(year, month, date);
}

const oneDay = 24 * 60 * 60 * 1000;
export function getRelativeDate(date, direction) {
  const newDate = new Date(date).getTime() + oneDay * direction;
  return new Date(newDate);
}

export function getFirstDate(year, month) {
  return new Date(`${year}/${month + 1}/01`);
}

export function getLastDate(year, month) {
  const nextFirst = getFirstDate(year, month).setMonth(month + 1);
  return getRelativeDate(nextFirst, -1);
}
