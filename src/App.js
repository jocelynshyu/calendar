import { useState, useCallback } from 'react';
import styled from 'styled-components';
import Title from './components/General/Title';
import Calendar from './components/Calendar';

const Main = styled.main`
  display: flex;
  justify-content: center;
`;

const Info = styled.p`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 30px 20px 0;
`;

function App() {
  const [text, setText] = useState('');

  const onChange = useCallback((start, end) => {
    setText(`${start || '?'} ~ ${end || '?'}`);
  }, []);

  return (
    <>
      <header>
        <Title>Calendar</Title>
      </header>
      <Main>
        <Calendar onChange={onChange} />
      </Main>
      <Info>
        <b>Result</b>
        <span>{text}</span>
      </Info>
    </>
  );
}

export default App;
